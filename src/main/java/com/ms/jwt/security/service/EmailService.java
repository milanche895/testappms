package com.ms.jwt.security.service;

import java.io.IOException;
import java.util.Date;

public interface EmailService {
	
	public void forgotPasswordMail(String to,String newPassword, String userName) throws  IOException;
	
	public void createdClient(String clientEmail,String password,Date date) throws  IOException;
	
	public void customerSupport(String email,String text) throws IOException;

}
