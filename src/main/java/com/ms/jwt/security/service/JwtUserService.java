package com.ms.jwt.security.service;

import java.util.List;

import com.ms.jwt.security.DTO.JwtUserDTO;

public interface JwtUserService {
	
	public List<JwtUserDTO> getAllUsers(String token);
	
	public List<JwtUserDTO> getAllVisibleUsers(String token, Boolean visible);
	
	public JwtUserDTO getUserById(Long id);
	
	public void deleteUser(Long id);
	
	public JwtUserDTO updateUser(JwtUserDTO jwtUserDTO); 
	
	public List<JwtUserDTO> getUserByUserNameContaining(String userName);

}
