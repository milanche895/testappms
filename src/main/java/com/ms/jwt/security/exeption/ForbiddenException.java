package com.ms.jwt.security.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "User not authorized")
public class ForbiddenException extends Exception{

	private static final long serialVersionUID = 1L;
	
	

}