package com.ms.jwt.security.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ms.jwt.security.entity.JwtUser;
import com.ms.jwt.security.entity.Token;
import com.ms.jwt.security.respository.JwtRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtGenerator {

	@Autowired
	public JwtRepository jwtRepository;
	//Ovde pravimo token
	public Token generate(JwtUser jwtUser) {
		// TODO Auto-generated method stub
		Token token = new Token("Token "+ Jwts.builder()
				.setSubject(jwtUser.getUserName())
		.claim("userId",jwtUser.getId())
		.claim("roleName",jwtUser.getRole().getRoleName())
		.claim("userName", jwtUser.getUserName())
		.signWith(SignatureAlgorithm.HS512, "youtube")
		.compact(), jwtUser.getRole().getRoleName(), jwtUser.getId(),jwtUser.getUserName());
		
	return	token;
	}
	
}

