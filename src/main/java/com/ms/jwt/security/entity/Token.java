package com.ms.jwt.security.entity;

public class Token {

	private String token;
	
	private String roleName;
	
	private Long id;
	
	private String userName;
	
	public Token(String token, String roleName, Long id, String userName) {
		
		this.token = token;
		this.roleName = roleName;
		this.id = id;
		this.userName = userName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
