package com.ms.jwt.security.DTO;

import com.ms.jwt.security.entity.JwtUser;

// parametri za User-a
public class JwtUserDTO {
	
	private Long id;

	private String userName;
	
	private String password;
	
	private String email;
	
	private String roleName;
	
	private boolean visible;
	
	public JwtUserDTO() {
		
	}

	public JwtUserDTO(JwtUser user) {
		this.id = user.getId();
		this.userName = user.getUserName();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.roleName = user.getRole().getRoleName();
		this.visible = user.getVisible();
		// TODO Auto-generated constructor stub
	} 
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
