package com.ms.jwt.security.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.jwt.security.entity.JwtUser;

@Repository
public interface JwtRepository extends JpaRepository<JwtUser, Long>{
	
	public JwtUser findOneByUserName(String userName);
	
	public JwtUser findOneById(Long Id);

	public List<JwtUser> findAllByVisible(Boolean visible);

	public List<JwtUser> findByUserNameContaining(String userName);

	public JwtUser findOneByEmail(String email);
	
}
