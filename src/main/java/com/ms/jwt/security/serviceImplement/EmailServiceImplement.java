package com.ms.jwt.security.serviceImplement;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Service;
import com.ms.jwt.security.service.EmailService;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

@Service
public class EmailServiceImplement implements EmailService{

	 
	
	@Override
	public void forgotPasswordMail(String to, String newPassword, String userName) throws  IOException {
		
	  
		Email from = new Email("noreply@msjwtsecurity.com");
	    String subject = "Forgot passwor email";
	    
	    Mail mail = new Mail();
	    mail.setFrom(from);
	    mail.setTemplateId("d-18a4887eab464aeba3e25518ddcbfba6");
	    mail.setSubject(subject);
	    Personalization personalization = new Personalization();
	    personalization.addDynamicTemplateData("subject", "Forgot password");
	    personalization.addDynamicTemplateData("password", newPassword);
	    personalization.addDynamicTemplateData("username", userName);
	    Email email = new Email(to);
	    personalization.addTo(email);
	    personalization.setSubject("Forgot password");
	    mail.addPersonalization(personalization);
	    
	    System.out.println(to);
	   
	    SendGrid sg = new SendGrid("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");
	    Request request = new Request();
	   
	    try {
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
	      System.out.println(response.getStatusCode());
	      System.out.println(response.getBody());
	      System.out.println(response.getHeaders());
	    } catch (IOException ex) {
	    	System.out.println("Bilo sta"+ex.getMessage());
	      throw ex;
	    }
		
	}

	@Override
	public void createdClient(String clientEmail,String password,Date date) throws  IOException {

		
		Email from = new Email("noreply@msjwtsecurity.com");	    
	    Mail mail = new Mail();
	    
	    Personalization personalization = new Personalization();

	    personalization.addDynamicTemplateData("email", clientEmail);
	    personalization.addDynamicTemplateData("password", password);
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    String strDate = dateFormat.format(date);   
	    personalization.addDynamicTemplateData("date", strDate);
	    personalization.addDynamicTemplateData("subject", "Welcome");
	    
	    personalization.addTo(new Email(clientEmail));
	    mail.setFrom(from);
	    mail.setTemplateId("d-18a4887eab464aeba3e25518ddcbfba6");
	    mail.addPersonalization(personalization);
	    mail.setSubject("Welcome");
	   
	    SendGrid sg = new SendGrid("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");
	    Request request = new Request();
	   
	    try {
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
	      System.out.println(response.getStatusCode());
	      System.out.println(response.getBody());
	      System.out.println(response.getHeaders());
	    } catch (IOException ex) {
	      throw ex;
	    }
	}


	@Override
	public void customerSupport(String email, String text) throws IOException {

		
		//password for customeremail : passwordcustomer
		
		Email from = new Email("noreply@msjwtsecurity.com");
	    String subject = email;
	    Email to = new Email("milanstojanovic895@gmail.com");
	    Content content = new Content("text/html", "Customer suport "+text);
	    Mail mail = new Mail(from, subject, to, content);

	    SendGrid sg = new SendGrid("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");
	    Request request = new Request();
	    try {
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
	      System.out.println(response.getStatusCode());
	      System.out.println(response.getBody());
	      System.out.println(response.getHeaders());
	    } catch (IOException ex) {
	      throw ex;
	    }
		
	}

}
