package com.ms.jwt.security.serviceImplement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.HttpClientErrorException.BadRequest;

import com.ms.jwt.security.DTO.JwtUserDTO;
import com.ms.jwt.security.entity.JwtUser;
import com.ms.jwt.security.entity.Role;
import com.ms.jwt.security.entity.Token;
import com.ms.jwt.security.exeption.BadRequestException;
import com.ms.jwt.security.exeption.ForbiddenException;
import com.ms.jwt.security.exeption.NotFoundException;
import com.ms.jwt.security.exeption.UnauthorizedException;
import com.ms.jwt.security.respository.JwtRepository;
import com.ms.jwt.security.respository.RoleRepository;
import com.ms.jwt.security.security.JwtGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Service
public class JwtUserServiceImplement {
		
	@Autowired
	public JwtRepository jwtRepository;
	
	@Autowired
	public JwtGenerator jwtGenerator;
	
	@Autowired
	public PermisionSystem permisionSystem;
	
	@Autowired 
	public RoleRepository roleRepository;
	
	public JwtUser changePassword(String email) {

		JwtUser user = jwtRepository.findOneByEmail(email);
		System.out.println(email);
		if(!Objects.isNull(user)) {
		String newPassword = UUID.randomUUID().toString();
				
		user.setPassword(newPassword);
		
		JwtUser userAfterUpdate = jwtRepository.save(user);
		
		return userAfterUpdate;
		
		}else{
			
			return null;
		}
		
		
		
	}
	
	public JwtUserDTO updateUserPassword(JwtUser jwtUser) {
		
		JwtUser user = jwtRepository.findOneByEmail(jwtUser.getEmail());
		
		user.setPassword(jwtUser.getPassword());
		
		jwtRepository.save(user);
		
		return new JwtUserDTO(user);
	}
	
	//Logika za login korisnika
	public Token loginUser(JwtUser jwtUser) throws NotFoundException, BadRequestException {
	
		JwtUser user = jwtRepository.findOneByUserName(jwtUser.getUserName());
		
	if(user == null) {
		
		throw new NotFoundException();
	
	}else {
		
		
		if(user.getPassword().equals(jwtUser.getPassword())) {
			
			Token oken = jwtGenerator.generate(user);
			
			return oken;		
			
			}else {
			
				throw new BadRequestException();	
			}
	
		}
		
	}
	public String registrationUser(JwtUserDTO jwtUserDTO) {
		
		JwtUser  user = new JwtUser();
		
		Role role = roleRepository.findOneById(2L);
		
		user.setRole(role);
		user.setUserName(jwtUserDTO.getUserName());
		user.setPassword(jwtUserDTO.getPassword());
		user.setEmail(jwtUserDTO.getEmail());
		user.setVisible(true);
		
		jwtRepository.save(user);
		
		return "Registrovan";
		
		
		
	}
	//Logika za uzimanje tokena korisnika i proveravanje kroz token da li je user ili admin
	public List<JwtUserDTO> getAllUsers(@RequestHeader("Authorisation") String token) throws ForbiddenException, NotFoundException{
		
		
		if (permisionSystem.checkAdminAccess(token)== true) {
			// Pocetno slovo objekta malo
			List<JwtUser> jwtUserList = jwtRepository.findAll();
			
			List<JwtUserDTO> jwtUserDtoList = new ArrayList<JwtUserDTO>();
			// for petlja za prolazak kroz listu
			for (JwtUser jwtUser : jwtUserList) {
				
				if (jwtUser.getVisible()== true || jwtUser.getVisible()== false ) {
					
					JwtUserDTO jwtUserDto = new JwtUserDTO();
					//Dodavanje user i role u objektu
					jwtUserDto.setId(jwtUser.getId());
					jwtUserDto.setUserName(jwtUser.getUserName());
					jwtUserDto.setPassword(jwtUser.getPassword());
					jwtUserDto.setEmail(jwtUser.getEmail());
					jwtUserDto.setRoleName(jwtUser.getRole().getRoleName());
					jwtUserDto.setVisible(jwtUser.getVisible());
					//dodeljujemo listi objekat
					jwtUserDtoList.add(jwtUserDto);
					
				}else {
					
					throw new NotFoundException();
				
				}
				
			}
			
			return jwtUserDtoList;
			
			} else {

				throw new ForbiddenException();
			
		}
		
		
	}
	public List<JwtUserDTO> getAllVisibleUsers(@RequestHeader("Authorisation") String token, Boolean visible) throws ForbiddenException, NotFoundException{
		
		
		if (permisionSystem.checkAdminAccess(token)== true) {
			// Pocetno slovo objekta malo
			List<JwtUser> jwtUserList = jwtRepository.findAllByVisible(true);
			
			List<JwtUserDTO> jwtUserDtoList = new ArrayList<JwtUserDTO>();
			// for petlja za prolazak kroz listu
			for (JwtUser jwtUser : jwtUserList) {
				
				if (jwtUser.getVisible()== true) {
					
					JwtUserDTO jwtUserDto = new JwtUserDTO();
					//Dodavanje user i role u objektu
					jwtUserDto.setId(jwtUser.getId());
					jwtUserDto.setUserName(jwtUser.getUserName());
					jwtUserDto.setPassword(jwtUser.getPassword());
					jwtUserDto.setEmail(jwtUser.getEmail());
					jwtUserDto.setRoleName(jwtUser.getRole().getRoleName());
					jwtUserDto.setVisible(jwtUser.getVisible());
					//dodeljujemo listi objekat
					jwtUserDtoList.add(jwtUserDto);
					
				}else {
					
					throw new NotFoundException();
				
				}
				
			}
			
			return jwtUserDtoList;
			
			} else {

				throw new ForbiddenException();
			
		}
		
		
	}
	public JwtUserDTO getUserById(@RequestHeader("Authorisation") String token, Long id) throws ForbiddenException, NotFoundException {
		
		if (permisionSystem.checkAdminAccess(token)== true) {
		
		JwtUser jwtUser = jwtRepository.findOneById(id);
		
			if (jwtUser.getVisible()== true) {
				
		return new JwtUserDTO(jwtUser);
		
			}else {
				
				throw new NotFoundException();
				
			}
			
		}else {
			
			throw new ForbiddenException();
		}
	}
	
	public void deleteUser(Long id, String token) throws ForbiddenException {
		
		if (permisionSystem.checkAdminAccess(token)== true) {
		
		JwtUser jwtUser = jwtRepository.findOneById(id);
		jwtUser.setVisible(false);
		jwtRepository.save(jwtUser);
		
		}else {
			
			throw new ForbiddenException();
		}
		
	}
	public JwtUserDTO updateUser(JwtUserDTO jwtUserDTO, String token) throws ForbiddenException, NotFoundException {
		
		if (permisionSystem.checkAdminAccess(token)== true) {
		
			JwtUser jwtUser = jwtRepository.findOneById(jwtUserDTO.getId());		
		
			if (jwtUser.getVisible()== true) {
		
				jwtUser.setId(jwtUserDTO.getId());
				jwtUser.setPassword(jwtUserDTO.getPassword());
				jwtUser.setUserName(jwtUserDTO.getUserName());
				jwtUser.setEmail(jwtUserDTO.getEmail());
				jwtUser.setVisible(true);

		
				jwtRepository.save(jwtUser);

		
				return new JwtUserDTO(jwtUser);
		
				}else {
			
				throw new NotFoundException();
			}
	
		
		} else {
		
		throw new ForbiddenException();
		
		}
	}
	
	public List<JwtUserDTO> getUserByUserNameContaining(@RequestHeader("Authorisation") String token, String userName) throws ForbiddenException, NotFoundException{
		
		
		if (permisionSystem.checkAdminAccess(token)== true) {
			// Pocetno slovo objekta malo
			List<JwtUser> jwtUserList = jwtRepository.findByUserNameContaining(userName);
			
			List<JwtUserDTO> jwtUserDtoList = new ArrayList<JwtUserDTO>();
			// for petlja za prolazak kroz listu
			for (JwtUser jwtUser : jwtUserList) {
				
				if (jwtUser.getVisible()== true) {
					
					JwtUserDTO jwtUserDto = new JwtUserDTO();
					//Dodavanje user i role u objektu
					jwtUserDto.setId(jwtUser.getId());
					jwtUserDto.setUserName(jwtUser.getUserName());
					jwtUserDto.setPassword(jwtUser.getPassword());
					jwtUserDto.setEmail(jwtUser.getEmail());
					jwtUserDto.setRoleName(jwtUser.getRole().getRoleName());
					jwtUserDto.setVisible(jwtUser.getVisible());
					//dodeljujemo listi objekat
					jwtUserDtoList.add(jwtUserDto);
					
				}else {
					
					throw new NotFoundException();
				
				}
				
			}
			
			return jwtUserDtoList;
			
			} else {

				throw new ForbiddenException();
			
		}
		
		
	}
	
}
