package com.ms.jwt.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ms.jwt.security.DTO.JwtUserDTO;
import com.ms.jwt.security.entity.JwtUser;
import com.ms.jwt.security.entity.Token;
import com.ms.jwt.security.exeption.BadRequestException;
import com.ms.jwt.security.exeption.ForbiddenException;
import com.ms.jwt.security.exeption.NotFoundException;
import com.ms.jwt.security.security.JwtGenerator;
import com.ms.jwt.security.serviceImplement.JwtUserServiceImplement;

@RestController
@RequestMapping("/user")
public class TokenController {
	
	@Autowired
	
	private JwtUserServiceImplement jwtUserService;
	
	private JwtUserServiceImplement jwtUserServiceImplement;
	
	private JwtGenerator jwtGenerator;
	
	public TokenController(JwtGenerator jwtGenerator) throws ForbiddenException{
		this.jwtGenerator = jwtGenerator;
	}
	//Api za proveru tokena
	@PostMapping("/login")
	public Token generate(@RequestBody JwtUser jwtUser) throws ForbiddenException, NotFoundException, BadRequestException{

		return jwtUserService.loginUser(jwtUser);
	}
	@PostMapping("/registration")
	public ResponseEntity <String> registrationUser(@RequestBody JwtUserDTO jwtUserDTO) throws ForbiddenException, NotFoundException{
		
		return new ResponseEntity<String>(jwtUserService.registrationUser(jwtUserDTO), HttpStatus.OK);
		
	}
	
	
}
