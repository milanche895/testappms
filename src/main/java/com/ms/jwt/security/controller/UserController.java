package com.ms.jwt.security.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.jwt.security.DTO.JwtUserDTO;
import com.ms.jwt.security.entity.JwtUser;
import com.ms.jwt.security.exeption.ForbiddenException;
import com.ms.jwt.security.exeption.NotFoundException;
import com.ms.jwt.security.respository.JwtRepository;
import com.ms.jwt.security.security.JwtAuthenticationProvider;
import com.ms.jwt.security.security.JwtGenerator;
import com.ms.jwt.security.service.JwtUserService;
import com.ms.jwt.security.serviceImplement.JwtUserServiceImplement;
import com.ms.jwt.security.service.EmailService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@RestController
@RequestMapping("/rest/hello")
public class UserController {
	

	//Kreiranje API za hello
	
	@Autowired 
	JwtUserServiceImplement jwtUserServiceImplement;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	JwtGenerator jwtGenerator;
	
	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@GetMapping("/users/hello")
	public String hello() {
		
	return "Page for user 'Hello World'";
	
	}
	@PutMapping("/users/forgotpassword")
	public ResponseEntity<String> resetPassword(@RequestBody  JwtUser user) {
		
		if(Objects.isNull(user)) {
		
			return new ResponseEntity<>("User ceredentionals are null" ,HttpStatus.BAD_REQUEST);
			
		}else {
		
		try {
		
			JwtUser userAfterUpdate = jwtUserServiceImplement.changePassword(user.getEmail());
			
			emailService.forgotPasswordMail(userAfterUpdate.getEmail(), userAfterUpdate.getPassword(),userAfterUpdate.getUserName());

			logger.debug("New mail has been sent");
			
		return new ResponseEntity<String>("Email is send with new password", HttpStatus.OK);
		}catch (IOException e) {
				
			logger.debug("New mail has not been sent : "+e.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
		
	}
	@PutMapping("users/newpassword")
	public ResponseEntity<JwtUserDTO> updatePassword(@RequestBody JwtUser user,@RequestHeader("Authorisation") String token) {
		
		return new ResponseEntity<JwtUserDTO>(jwtUserServiceImplement.updateUserPassword(user), HttpStatus.OK);
		
	}
	
	//Kreiranje API za hello/users 
	@GetMapping("/users")
	public ResponseEntity<List<JwtUserDTO>> getAllUsers(@RequestHeader("Authorisation") String token) throws ForbiddenException, NotFoundException{
		
		return new ResponseEntity<List<JwtUserDTO>>(jwtUserServiceImplement.getAllUsers(token), HttpStatus.OK);
		
	}
	
	@GetMapping("/users/active")
	public ResponseEntity<List<JwtUserDTO>> getAllVisibleUsers(@RequestHeader("Authorisation") String token,@RequestParam(name="visible") Boolean visible) throws ForbiddenException, NotFoundException{
		
		return new ResponseEntity<List<JwtUserDTO>>(jwtUserServiceImplement.getAllVisibleUsers(token, visible), HttpStatus.OK);
		
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<JwtUserDTO> getUserById(@RequestHeader("Authorisation") String token, @PathVariable(name = "id") Long id) throws ForbiddenException, NotFoundException{
		
		return new ResponseEntity<JwtUserDTO>(jwtUserServiceImplement.getUserById(token, id), HttpStatus.OK);
	
	}
	@DeleteMapping("users/{id}")
	public ResponseEntity<String> deleteUser(@RequestHeader("Authorisation") String token, @PathVariable(name = "id") Long id) throws ForbiddenException{
		
		jwtUserServiceImplement.deleteUser(id, token);
		
		return new ResponseEntity<String>("User delete", HttpStatus.OK);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<JwtUserDTO> updateUser(@RequestBody JwtUserDTO jwtUserDTO, @RequestHeader("Authorisation") String token, @PathVariable(name = "id") Long id) throws ForbiddenException, NotFoundException{
		
		jwtUserDTO.setId(id);
	
		return new ResponseEntity<JwtUserDTO>(jwtUserServiceImplement.updateUser(jwtUserDTO, token),HttpStatus.OK);
			
	}
	@GetMapping("/users/search")
	public ResponseEntity<List<JwtUserDTO>> getUserByUserNameContaining (@RequestHeader("Authorisation") String token, @RequestParam(name="userName", required = false) String userName) throws ForbiddenException, NotFoundException{
		
		return new ResponseEntity<List<JwtUserDTO>>(jwtUserServiceImplement.getUserByUserNameContaining(token, userName), HttpStatus.OK);
		
	}
}
