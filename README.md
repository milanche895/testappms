# About project

## Installation
Clone the BitBucket repository using GitBush

> $ git clone git clone https://milanche895@bitbucket.org/milanche895/testappms.git

##Heroku testApp

> https://glacial-mesa-80747.herokuapp.com/user/login "login user({"userName":"milanche","password":"milanche"})"  POST
> https://glacial-mesa-80747.herokuapp.com/user/registration "register user({"userName":"milanche","password":"milanche"})"  POST
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/hello "Page for user "Hello World""                                                GET
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users  "AllUser"  
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/active?visible=true  "AllVisibleUsers"                                            GET
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/{id}  "UserById"                                       GET
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/search?userName={} "UserByUserNameContaining"          GET
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/{id} "Delete user"                                     DELETE
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/{id} "Update user"                                     PUT
> https://glacial-mesa-80747.herokuapp.com/rest/hello/users/forgotpassword                                     PUT

## Build with 

* Spring Tool Suite 4
 >Installing and open Spring Tool Suite.Run project by right click on project folder and click RunAs > Spring Boot App
 
* pg Admin III
>Installing and open pgAdmin. Click File > Add Server and insert server data.  

 

